import os


def can_be_converted_to_digit(input):
    if isinstance(input, str) and len(input) == 1:
        return True

def convert_to_integer(input):
    return int(input)

if __name__ == "__main__":
    input_ = os.environ.get('input')
    if can_be_converted_to_digit(input_):
        print(convert_to_integer(input_))
    else:
        print('Can not be converted')