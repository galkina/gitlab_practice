import os
import psycopg2

def connection_to_db(postgres_url: str, query: str):
    try:
        connection = psycopg2.connect(postgres_url)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()
        return result
    except Exception as e:
        print(e)

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


if __name__ == '__main__':
    db_url = os.environ.get('postgres_url')
    query_ = '''SELECT
    Students.FirstName,
    Students.LastName,
    RecordBooks.Subject,
    RecordBooks.Grade
FROM
    Students
JOIN
    RecordBooks ON Students.StudentID = RecordBooks.StudentID
WHERE
    Students.StudentID = (
        SELECT
            StudentID
        FROM
            (
                SELECT
                    StudentID,
                    AVG(Grade) AS avg_grade
                FROM
                    RecordBooks
                GROUP BY
                    StudentID
                ORDER BY
                    avg_grade
                LIMIT 1
            ) AS subquery
    );'''
    result = connection_to_db(postgres_url=db_url, query=query_)
    print(result)
