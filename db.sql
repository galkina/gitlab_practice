CREATE TABLE IF NOT EXISTS RecordBooks (
    RecordID SERIAL PRIMARY KEY,
    RecordBookID INT NOT NULL,
    StudentID INT NOT NULL,
    Subject VARCHAR(50) NOT NULL,
    ExamDate DATE NOT NULL,
    Grade INT NOT NULL,
    TeacherFullName VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS Students (
    StudentID SERIAL PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    BirthDate DATE NOT NULL,
    RecordBookID INT NOT NULL
);

INSERT INTO RecordBooks (RecordBookID, StudentID, Subject, ExamDate, Grade, TeacherFullName)
VALUES
    (101, 1, 'Математика', '2023-12-26', 5,'Иванова А.А.'),
    (102, 2, 'Математика', '2023-12-26', 4,'Иванова А.А.'),
    (103, 3, 'Математика', '2023-12-26', 3,'Иванова А.А.'),
    (101, 1, 'Физика', '2023-12-28', 4,'Петров Б.Б.'),
    (102, 2, 'Физика', '2023-12-28', 5,'Петров Б.Б.'),
    (103, 3, 'Физика', '2023-12-28', 2,'Петров Б.Б.'),
    (101, 1, 'Программирование', '2023-12-30', 5,'Семенова А.А.'),
    (102, 2, 'Программирование', '2023-12-30', 5,'Семенова А.А.'),
    (103, 3, 'Программирование', '2023-12-30', 5,'Семенова А.А.');

INSERT INTO Students (FirstName, LastName, BirthDate, RecordBookID)
VALUES
    ('Юлия', 'Плешкова', '2002-11-25', 101),
    ('Анна', 'Кочемасова', '2002-07-18', 102),
    ('Алиса', 'Бармина', '2002-07-04', 103);
